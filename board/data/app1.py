import dash
import dash_table
import pandas as pd


df = pd.read_csv('data_dropout_59-64.csv')


app = dash.Dash()
app.layout = dash_table.DataTable(
    id='table',
    columns=[
         {"name": "STUDENT_ID", "id": "STUDENT_ID"},
        {"name": "ปีที่เข้า", "id": "ADMIT_YEAR"},
        {"name": "เกรดปี1เทอม1", "id": "เกรดปี1เทอม1"},
        {"name": "เกรดปี1เทอม2", "id": "เกรดปี1เทอม2"},
        {"name": "เกรดปี1เทอม3", "id": "เกรดปี1เทอม3"},
        {"name": "เกรดปี2เทอม1", "id": "เกรดปี2เทอม1"},
        {"name": "เกรดปี2เทอม2", "id": "เกรดปี2เทอม2"},
        {"name": "เกรดปี2เทอม3", "id": "เกรดปี2เทอม3"},
        {"name": "เกรดปี3เทอม1", "id": "เกรดปี3เทอม1"},
        {"name": "เกรดปี3เทอม2", "id": "เกรดปี3เทอม2"},
        {"name": "เกรดปี3เทอม3", "id": "เกรดปี3เทอม3"},
        {"name": "เกรดปี4เทอม1", "id": "เกรดปี4เทอม1"},
        {"name": "เกรดปี4เทอม2", "id": "เกรดปี4เทอม2"},
        {"name": "เกรดปี4เทอม3", "id": "เกรดปี4เทอม3"},
        {"name": "GPA_SCHOOL ม.6", "id": "GPA_SCHOOL"},
       
    ],
    data=df.to_dict('records')
)
if __name__ == '__main__':
    app.run_server(debug=True)
